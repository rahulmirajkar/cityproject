# -*- coding: utf-8 -*-

#!/usr/bin/python3

tup1 = ('physics', 'chemistry', 1997, 2000)
tup2 = (1, 2, 3, 4, 5, 6, 7 )

print ("tup1[0]: ", tup1[0])
print ("tup2[1:5]: ", tup2[1:5])

#update the tuples

tup3 = (12, 34.56)
tup4 = ('abc', 'xyz')

# Following action is not valid for tuples
# tup1[0] = 100;

# So let's create a new tuple as follows
tuplenew = tup3 + tup4
print (tuplenew)

# delete some tuple

tupl = ('physics', 'chemistry', 1997, 2000);

del tupl;
print ("After deleting tup : ")
print (tupl)  # after successful deletion it throws an exception
