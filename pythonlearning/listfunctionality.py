# -*- coding: utf-8 -*-

list1 = ['physics', 'chemistry', 1997, 2000]
list2 = [1, 2, 3, 4, 5, 6, 7 ]

# access the list values
print ("list1[0]: ", list1[0])
print ("list2[1:5]: ", list2[1:5])

# update the list values
list1[2] = 2001
print ("New value available at index 2 : ", list1[2])

# delete one element from the list
del list1[2]
print ("After deleting value at index 2 : ", list1)


# max element in list 
print ("max element in the list", max(list2))

# min element in list 
print ("min element in the list", min(list2))

# To find length of list
print("length of list", len(list2))
