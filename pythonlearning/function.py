# -*- coding: utf-8 -*-

# function definition
def sum( arg1, arg2):
    
    total = arg1 + arg2
    print ("Inside the function:", total)
    return total


# function call

total = sum(15,20)
print ("Outside the function", total)


