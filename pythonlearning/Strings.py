# -*- coding: utf-8 -*-

var1 = 'Hello World!'
var2 = "Python Programming"

print ("var1[0]: ", var1[0])
print ("var2[1:5]: ", var2[1:5])

# updating the var1 with other string

print ("Updated String :- ", var1[:6] + 'Rahul')
