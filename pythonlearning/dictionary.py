# -*- coding: utf-8 -*-

dict1 = {'Name': 'Zara', 'Age': 7, 'Class': 'First'}

# access a dictionary based on keys
print ("dict1['Name']: ", dict1['Name'])
print ("dict1['Age']: ", dict1['Age'])

# return the dictionary in terms of (key,value) list
print( dict1.items())

# try to access element not in dictionary
print ("dict1['Rahul']: ", dict['Rahul'])

