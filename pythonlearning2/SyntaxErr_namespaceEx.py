# -*- coding: utf-8 -*-

var1 = 5

def func1():
    
    #var2 in the local namespace(with in the function)
    global var1
    var2 = 3
    var3 = var1 + var2 #accessing the global variable var1
    print var3 # SyntaxError should occur here.
 
func1()

