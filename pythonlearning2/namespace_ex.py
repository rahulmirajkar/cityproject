# -*- coding: utf-8 -*-

# defining the variable in global namespace

var1 = 5

def func1():
    
    #var2 in the local namespace(with in the function)
    global var1
    var2 = 3
    var3 = var1 + var2
    print (var3)
 
func1()
        

