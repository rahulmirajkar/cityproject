# -*- coding: utf-8 -*-

# identity operator 
# True if both the operators are true
x1 = 5
y1 = 5

x2 = 'Hello'
y2 = 'Hello'

x3 = [1,2,3,4]
y3 = [1,2,3,4]

print (x1 is not y1)

print (x2 is y2)

print (x3 is y3)

# membership operator
print ('H' in x2)
print ('a' not in x3)
