# -*- coding: utf-8 -*-

class Names:
  
    # default constructor 
    def __init__(self): 
        self.geek = "GeekforGeeks"
  
    # a method for printing data members 
    def print_Names(self): 
        print(self.geek) 
  
  
# creating object of the class 
obj = Names() 
  
# calling the instance method using the object obj 
obj.print_Names()
