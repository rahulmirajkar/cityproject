# -*- coding: utf-8 -*-

# accessing the string
name = "RAHUL"

# print the characters of string based on index
print (name[0])
print (name[-1])

# string slicing
print (name[slice(3)])

# slice(start, stop, step)
print (name[slice(1,4,1)])

# reverse the string
print (name[::-1])