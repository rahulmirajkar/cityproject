# -*- coding: utf-8 -*-

# illustrate functions can be treated as objects

def func1(text): 
    return text.upper() 
  
print (func1('first function')) 

# first class function
# assigning function to variables  
func2 = func1 
  
print (func2('second function'))
