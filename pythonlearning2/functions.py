# -*- coding: utf-8 -*-

# function that returns the sum of first 10 numbers
# function defn
def sum():
    num = 0
    for i in range(1,11):
        num += i
    return num


# function call
result = sum()
print (result)

    
