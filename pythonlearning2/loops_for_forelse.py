# -*- coding: utf-8 -*-

# illustration of for and forelse loops

# iterating over a list
print ("List Iteration")
list = ["learning", "Python", "advance topics"]
for i in list:
    print(i)
    
# iterating over a string
print ("String Iteration")
name = "Rahul"
for i in name:
    print(i)
    
print("\n For else loop")
for i in name:
    print(i)
else:
    print("No Break")
    
    
for i in name:
    print(i)
    break
else:
    print("No Break")
    
    
    