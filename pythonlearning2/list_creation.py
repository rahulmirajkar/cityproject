# -*- coding: utf-8 -*-

# Creating a List   
List = []   
print(List) 

# Create a list of strings 
List = ['PythonLearning', 'Indepth']  
print(List)

# Creating a Multi-Dimensional List   
List = [['PythonLearning', 'Phase2'], ['Indepth']]   
print(List) 