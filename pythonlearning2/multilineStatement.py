# -*- coding: utf-8 -*-

# multiline statement illustration

# declaration using the continuous character (\):
sum = 1 + 2 + 3 \
      +4 + 5 + 6 \
      + 7 + 8 + 9
        
# declaration using paranthesis ():
n = (1 + 2 * 3 + 4 + 5 * 6)

# declaration using square brackets []:
languages = ['JAVA',
             'JAVASCRIPT',
             'PYTHON',
             'RUBY',
             'PEARL']

# declaration using braces {}:
total = { 1 + 2 + 3 + 4 +
         5 + 6 + 7 + 8 + 9 }

#declaration using semicolons (;):
flag = 2; ropes = 3; pole = 4

print (sum)
print (n)
print (languages)
print (total)
