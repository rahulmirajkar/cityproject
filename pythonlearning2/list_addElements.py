# -*- coding: utf-8 -*-

# illustration using append(), insert(), extend()

#create a list
List = []

# use append() func
List.append(2)
List.append(3)

# use insert() func
List.insert(3, 12)
List.insert(0,"Pythonlearning2")

# use extend() func
List.extend([8, 'Python', 'Indepth'])

#print List content
print (List)