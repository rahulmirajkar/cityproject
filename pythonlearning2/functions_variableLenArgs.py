# -*- coding: utf-8 -*-

# variable args
def func1(*argv):
    for arg in argv:
        print (arg, end = " ")
        
# variable keyword args
def func2(**kwargs):
    for key, value in kwargs.items():
        print("%s == %s" %(key, value))
        
func1('hello', 'welcome')
print()
func2(first = 'python', mid = 'learning2', last = 'In depth')
