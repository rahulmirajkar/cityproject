# -*- coding: utf-8 -*-

# create a Set with name set1
set1 = set() 
      
# Adding to the Set using add()  
set1.add(8) 
set1.add((6, 7)) 
print(set1)   
    
# Additio to the Set using Update()    
set1.update([10, 11]) 
print(set1) 
