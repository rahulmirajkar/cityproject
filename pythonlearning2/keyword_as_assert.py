# -*- coding: utf-8 -*-

# assert keyword

x = "rahul"

assert x == "python" #if conditions becomes true, nothing happens

#if condition becomes false, Assersion error is raised.
assert x == "end"


