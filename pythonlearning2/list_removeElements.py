# -*- coding: utf-8 -*-

# elements from the list can be removed
# using remove() & pop() functions

# List 
List = [1,3,2,3,4,6,7,8]

List.remove(3)
print (List)

List.pop()
print (List)

List.pop()
print ("newList", List)
