# -*- coding: utf-8 -*-

# creating a dictionary
Dict = {1:'rahul', 'name':'For', 3: 'perfection'}

# access an element in dictionary
print (Dict['name'])

# access element using get() function
print (Dict.get(3))

# remove element from the dictionary
Dict1 = { 5: 'welcome', 6: 'To', 7: 'python',
         'A':{1: 'learning', 2: 'with', 3:'examples'},
         8: 'Delete this entry'
         }

# using pop()
Dict1.pop(6)
print (Dict1)

# using popitem()
Dict1.popitem()
print (Dict1)
