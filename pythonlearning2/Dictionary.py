# -*- coding: utf-8 -*-

# create empty dictionary
Dict = {} 
print(Dict)

# with Integer Keys   
Dict = {1: 'Python', 2: 'For', 3: 'learning2'} 
print(Dict)

# with Mixed keys   
Dict = {'Name': 'Python', 1: [1, 2, 3, 4]} 
print(Dict)

# Nested dictionary
Dict1 = {1: 'Python', 2: 'For',   
        3:{'A' : 'Welcome', 'B' : 'To', 'C' : 'Indepth'}}  
    
print(Dict1)