# -*- coding: utf-8 -*-

x = 5
y = 3

# arithmetic operators usage
print (x + y) # addition

print (x - y) # substraction

print (x * y) # multiplication

print (x / y) # division

print (x % y) # Modulus

print (x ** y) # similar to 5^3 = 5*5*5 (exponential)

print (x // y) # floor division
