# -*- coding: utf-8 -*-

# illustration to demonstrate 
# while and while-else loop

i = 0
while (i < 3):
    i = i + 1
    print ("Hello")
    
# create a list 
# and check if the list contains any element

a = [1, 2, 3, 4]
while a:
    print (a.pop())

i = 10
while i < 12:
    i += 1 # i = i + 1
    print (i)
    break

else: # doesnot gets executed because of break
    print ("No Break")
    