# -*- coding: utf-8 -*-

# python and keyword; logical operator which combines conditional statements
num1 = 2

x = (num1 > 1 and num1 < 5)

print (x)

if (num1 > 1 and num1 < 10):
    print("Both the conditions are True")

else:
    print("Either one of this condition is False")