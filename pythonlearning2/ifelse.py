# -*- coding: utf-8 -*-

num = 3
if num > 0:
    print(num, "is a positive number")
print("This is printed.")

num = -1
if num > 0:
    print(num, "is a positive number")
print("This is also printed.")
