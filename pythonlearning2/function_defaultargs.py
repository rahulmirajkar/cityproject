# -*- coding: utf-8 -*-

# illustrates the default args

def func(x, y = 50):
    print ("x: ", x)
    print ("y: ", y)
    
# func call
func(10)

# func call other
func(10, 30)
