# -*- coding: utf-8 -*-

class Person:
    name = "rahul"
    job = "engineer"
    location = "bangalore"
    project = "citi API"
    

# creating an object p1 for class Person
p1 = Person()

#accessing the class members using the object p1 and dot operator
print (p1.name)
print (p1.job + "," + p1.location)