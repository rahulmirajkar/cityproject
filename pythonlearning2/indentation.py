# -*- coding: utf-8 -*-

# Python program showing 
# indentation 
  
site = 'google'
  
if site == 'google': 
    print('Logging on to google and search python examples...') 
else: 
    print('retype the URL.') 
print('All set correctly!') #not indented since it does not belong to else block.
