# -*- coding: utf-8 -*-

# program illustrating the removing of the set elements
#create a Set of numbers
set1 = set([1, 2, 3, 4, 5, 6,    
            7, 8, 9, 10, 11, 12])   
  
# using Remove() method   
set1.remove(5)   
set1.remove(6) 
print(set1)   
  
# using Discard() method   
set1.discard(8)   
set1.discard(9) 
print(set1)   
  
# Set using the pop() method   
set1.pop() 
print(set1)   
  
# Set using clear() method   
set1.clear() 
print(set1)
