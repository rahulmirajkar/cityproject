# -*- coding: utf-8 -*-

# elements of list can be accessed using []
# that is using indexing; starts from 0
# can access positive as well as negative indexes 

# list
List = [1,2,3,4,5,6]

# accessing any elements 
print (List[0])
print (List[2])

# negative index 
# elements from last can be accessed.
print (List[-3])
print (List[-5])

# print the whole list
print (List)