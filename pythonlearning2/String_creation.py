# -*- coding: utf-8 -*-

# python program for string creation

# string using single quotes
print ('Learning the python in depth')

# string using double quotes
print ("Topic - String creation program")

# string using triple quotes
print ('''String creation using the triple quotes - executed''')